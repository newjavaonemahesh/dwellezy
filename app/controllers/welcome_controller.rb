class WelcomeController < ApplicationController
  def index
    @operations = Operation.all
    @listing_types = ListingType.all
    @locations = Location.all
  end
end
