Rails.application.routes.draw do

  get 'welcome/index'

  resources :operations, only: [:index]
  root 'welcome#index'
end
